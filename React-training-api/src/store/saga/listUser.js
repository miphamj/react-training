import { takeLatest, all, call, put } from 'redux-saga/effects';
import { getUser as getUserApi, editUser as editUserApi, delUser as delUserApi, addUser as addUserApi } from '../api/listUser';

function* addUser({ type, payload }) {
    const res = yield call(addUserApi, payload);
    if(res.status === 200){
        yield put({ type: 'users/ADD_USER', payload});
    }
}

function* editUser({ type, payload}) {
    const res = yield call(editUserApi, payload.userId, payload.user);
    if(res.status === 200){
        yield put({ type: 'users/EDIT_USER', payload });
    }
}

function* delUser({ type, payload}) {
    const res = yield call(delUserApi, payload);
    if(res.status === 200){
        yield put({ type: 'users/EDIT_USER', payload });
    }
}

function* getUser({ type, payload}) {
    const res = yield call(getUserApi, payload);
    console.log('res: ',res);
    if(res.status === 200){
        yield put({ type: 'users/ADD_USER', payload: res.data });
    }
}

export default [
    function* fetchWatcher() {
        yield all([
            takeLatest('users/ADD_USER', addUser),
            takeLatest('users/EDIT_USER', editUser),
            takeLatest('users/DEL_USER', delUser),
            takeLatest('users/', getUser)
        ])
    }
]
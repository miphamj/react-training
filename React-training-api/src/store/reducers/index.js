import { combineReducers } from 'redux';
import auth from './auth';
import listUser from './listUser'

export default combineReducers({
  auth,
  listUser
});

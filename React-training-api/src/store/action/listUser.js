
export const addUser = payload => ({
    type: 'users/ADD_USER',
    payload
})

export const editUser = (userId, user) => ({
    type: 'users/EDIT_USER',
    payload: {
        userId,
        user
    }
})

export const delUser = payload => ({
    type: 'users/DEL_USER',
    payload
})

export const getUser = () => ({
    type: 'users/'
})


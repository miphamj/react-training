import {create} from 'apisauce'

const API = create({
    baseURL: 'http://localhost:5000',
    timeout: 30000
});

export { API }; 
import React from "react";
import { connect } from "react-redux";
import { addUser } from '../store/action/listUser'

class Form extends React.Component {
    constructor(props){
        super(props);
        this.state={
        txtName: '', txtBirthdate: '', txtGender: ''
     }
    }
     
    onChangeInput = event => {
        
        this.setState({
            [event.target.name]: event.target.value
        }, ()=>{
            console.log(this.state)

        });
    }

    onAddUser = event => {
        event.preventDefault();
        this.props.addUser(this.state);
    };

    render() {
        return (
            <form onSubmit={this.onAddUser}>
                <div className="form-group">
                    <label>User</label>
                    <input
                        name="txtName"
                        type="text"
                        className="form-control"
                        id=""
                        placeholder="Name"
                        onChange={this.onChangeInput}
                    />
                    <input
                        name="txtBirthdate"
                        type="text"
                        className="form-control"
                        id=""
                        placeholder="date of birth"
                        onChange={this.onChangeInput}
                    />
                    <input
                        name="txtGender"
                        type="text"
                        className="form-control"
                        id=""
                        placeholder="gender"
                        onChange={this.onChangeInput}
                    />
                </div>
                <button type="submit" className="btn btn-primary">
                    Submit
        </button>
            </form>
        );
    }
}

export default connect(null, { addUser })(Form);
